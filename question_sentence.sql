insert into quizzes values("1", "ジブリ問題：「紅の豚」の主人公ポルコ・ロッソが乗っていた戦闘用飛行艇の名前はどれ？");
insert into quizzes values("2","宮崎駿監督の作品はどれ？ ");
insert into quizzes values("3", "「千と千尋の神隠し」の英語タイトルは？");
insert into quizzes values("4", "「となりのトトロ」の舞台設定になった場所がある市町村はどこ？");
insert into quizzes values("5", "ドラえもん問題：「ドラえもんの身長と体重は？");
insert into quizzes values("6", "「ドラえもん」が現在のテレビ朝日の前に放送されていたテレビ局はどこでしょう？");
insert into quizzes values("7", "ドラえもんは元々何色だったでしょか？");
insert into quizzes values("8", "スネ夫の実弟の名前は、何というでしょうか？");
insert into quizzes values("9", "ポケモンゲームは通常の最高何倍のダメージを相手に与える事ができる？");
insert into quizzes values("10", "銀魂問題:坂田銀時の病名は？");
insert into quizzes values("11", "ポケモン問題：時を司るポケモンは？");
insert into quizzes values("12", "空間を司るポケモンは？");
insert into quizzes values("13", "反転世界の主はだれ？");
insert into quizzes values("14", "ガンダムの母船「ホワイトベース」。ジオン公国軍からなんと呼ばれているか？");
insert into quizzes values("15", "シャアがセイラに贈った金塊。添えられた手紙には何と書かれていた？");
insert into quizzes values("16", "シャアのフルネームは？");
insert into quizzes values("17", "アムロの名台詞「二度もぶった！父親にもぶたれた事ないのに！」最初にぶたれて箇所と二度目にぶたれた箇所は何所でしょう");
insert into quizzes values("18", "映画銀河鉄道９９９でガラスのクレアは最後どうなった？");
insert into quizzes values("19", "メーテルの父親の名前は？");




insert into answers values ("1", "1. カーチスR3C-0", "1","0");
insert into answers values ("2", "2. サボイアS-21", "1","1");
insert into answers values ("3", "3. マッキ M.33", "1","0");
insert into answers values ("4", "4. YS－11", "1","0");

insert into answers values ("5", "1. 火垂るの墓", "2","0");
insert into answers values ("6", "2. おもひでぽろぽろ", "2","0");
insert into answers values ("7", "3. 耳をすませば", "2","0");
insert into answers values ("8", "4. On your Mark", "2","1");
 
insert into answers values ("9", "1. Spirited Away", "3","1");
insert into answers values ("10", "2. Hide-and-seek", "3","0");
insert into answers values ("11", "3. Hideaway", "3","0");
insert into answers values ("12", "4. ukiyaki", "3","0");

insert into answers values ("13", "1. 千葉県勝浦市", "4","0");
insert into answers values ("14", "2. 長野県諏訪市", "4","0");
insert into answers values ("15", "3. 北海道釧路市", "4","0");
insert into answers values ("16", "4. 埼玉県所沢市", "4","1");

insert into answers values ("17", "1. 119.3cmと128.1kg ", "5","0");
insert into answers values ("18", "2. 129.3cmと126.5kg", "5","0");
insert into answers values ("19", "3. 129.3cmと126.5kg", "5","0");
insert into answers values ("20", "4. 129.3cmと129.3kg", "5","1");

insert into answers values ("21", "1. フジテレビ", "6","0");
insert into answers values ("22", "2. TBSテレビ", "6","0");
insert into answers values ("23", "3. テレビ東京", "6","0");
insert into answers values ("24", "4. 日本テレビ ", "6","1");

insert into answers values ("25", "1. 緑 ", "7","0");
insert into answers values ("26", "2. 白", "7","0");
insert into answers values ("27", "3. 赤", "7","0");
insert into answers values ("28", "4. 黄", "7","1");

insert into answers values ("29", "1. スネ夫に弟はいない", "8","0");
insert into answers values ("30", "2. スネ太", "8","0");
insert into answers values ("31", "3. スネツグ", "8","1");
insert into answers values ("32", "4. スナ夫", "8","0");

insert into answers values ("33", "1. 36倍", "9","1");
insert into answers values ("34", "2. 12倍", "9","0");
insert into answers values ("35", "3. 8倍", "9","0");
insert into answers values ("36", "4. 24倍", "9","0");

insert into answers values ("37", "1. 痛風", "10","0");
insert into answers values ("38", "2. 糖尿病", "10","1");
insert into answers values ("39", "3. 高血圧", "10","0");
insert into answers values ("40", "4. 逆流性胃腸炎", "10","0");

insert into answers values ("41", "1. 虹色ポケモン ホウオウ", "11","0");
insert into answers values ("42", "2. 時渡りポケモン セレビィ", "11","0");
insert into answers values ("43", "3. 知識ポケモン ユクシー", "11","0");
insert into answers values ("44", "4. 時間ポケモン ディアルガ", "11","1");

insert into answers values ("45", "1. 巨大ポケモン レジギガス", "12","0");
insert into answers values ("46", "2. 空間ポケモン パルキア", "12","1");
insert into answers values ("47", "3. 想像ポケモン アルセウス", "12","0");
insert into answers values ("48", "4. はんこつポケモン ギラティナ", "12","0");

insert into answers values ("49", "1. 時間ポケモン ディアルガ", "13","0");
insert into answers values ("50", "2. 想像ポケモン アルセウス", "13","0");
insert into answers values ("51", "3. はんこつポケモン ギラティナ", "13","1");
insert into answers values ("52", "4. 空間ポケモン パルキア", "13","0");

insert into answers values ("53", "1. 白馬 ", "14","0");
insert into answers values ("54", "2. ブライト", "14","0");
insert into answers values ("55", "3. トロイ", "14","0");
insert into answers values ("56", "4. 木馬", "14","1");

insert into answers values ("57", "1. 優しきアルテイシア・ソム・ダインクンへ", "15","1");
insert into answers values ("58", "2. 美しきアルテイシア・ソム・ダインクンへ", "15","0");
insert into answers values ("59", "3. 愛しきアルテイシア・ソム・ダインクンへ", "15","0");
insert into answers values ("60", "4. 麗しきアルテイシア・ソム・ダインクンへ", "15","0");

insert into answers values ("61", "1. シャア・アズナブル・ホワイト", "16","0");
insert into answers values ("62", "2. シャア・アズ・ナブル", "16","0");
insert into answers values ("63", "3. アズナブ・ル・シャア ", "16","0");
insert into answers values ("64", "4. シャア・アズナブル", "16","1");

insert into answers values ("65", "1. 右頬と左頬", "17","0");
insert into answers values ("66", "2. 左頬と左頬", "17","0");
insert into answers values ("67", "3. 右頬と左頬", "17","0");
insert into answers values ("68", "4. 左頬と右頬", "17","1");

insert into answers values ("69", "1. 生身の体に戻って地球に帰る ", "18","0");
insert into answers values ("70", "2. 旅の最後まで無事に一緒", "18","0");
insert into answers values ("71", "3. 途中の星で降りてしまう", "18","0");
insert into answers values ("72", "4. プロメシュームを道連れに死んでしまう", "18","1");

insert into answers values ("73", "1. ドクター・ワン ", "19","0");
insert into answers values ("74", "2. ドクター・ゼロ", "19","0");
insert into answers values ("75", "3. ドクター・スリー", "19","0");
insert into answers values ("76", "4. ドクター・バン", "19","1");
