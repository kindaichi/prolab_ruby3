DROP TABLE IF EXISTS users;
CREATE TABLE users (
id          varchar(50) not null primary key,
name        varchar(50) not null,
age         int         not null,
percentage  int);
