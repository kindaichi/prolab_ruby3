
drop table if exists quizzes;
drop table if exists answers;

create table quizzes(
  id         varchar(50)  not null   primary key,
  question   text         not null);

create table answers(
  id           varchar(50)  not null primary key,
  choices      text         not null,
  quiz_id      varchar(50)  not null,
  correct      varchar(50)  not null,
  foreign key (quiz_id) references quizzes(id) );

/* create table correct_answers(
  id           varchar(50)  not null primary key,
  choices */

PRAGMA foreign_keys = true;
