# coding: utf-8
require 'rubygems'
require 'dbi'

# 問題文&解答文を出力し解答結果の正答率をデータベースに保存するまでの流れ

class Quiz
  def initialize(dbh)
    @dbh = dbh
    @question_id = ""
    @correct_number = ""
    @correct_counts = 0
    @quiz_counts = 0
  end

  # データベースから問題文を取ってくる処理
  def question
      sth = @dbh.execute("select id, question from quizzes; ")
      sth.each { |row|
        row.each_with_name { |val, name|
          puts `clear`
          if name == "question" then
            puts "#{val}"
          elsif name == "id" then
            @question_id = val  
          end
        }
          answer
          answer_choice
          @quiz_counts += 1
          gets        
      }

        # 正答率のメソッド挿入
        puts "-----------------------------------------------
あなたの正解率は#{percentage.to_i}％
正解数: #{@correct_counts}
問題数: #{@quiz_counts}
-----------------------------------------------"
        # 正答率結果をデータベースに保存する処理
        sth.finish
        sth = @dbh.do("update users set percentage = #{percentage} where id = '#{$id}';")
        gets
  end

  #　解答文をテーブルから取ってくる処理
  def answer
    sth = @dbh.execute("select choices, correct from answers where quiz_id = '#{@question_id}';")
    sth.each { |row|
      choice = ""
      correct = ""
      row.each_with_name { |val, name|
        if name == "choices" then
          choice = val
        elsif name == "correct" then
          correct = val
        end       
      }
      puts choice
      if correct == "1" then
        @correct_number = choice[0,1]
      end
    }
    sth.finish
  end

 # 正誤判定をする処理
  def answer_choice
    num2 = gets.chomp
    if num2 == @correct_number then
      # 正解の時の処理
      @correct_counts += 1
      puts "正解です"
    else
      #不正解の時の処理
      puts "不正解です"
    end
  end

 #正答率を計算する処理
  def percentage
    return (@correct_counts.to_f / @quiz_counts.to_f) * 100
  end

end

