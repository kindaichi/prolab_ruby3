#coding: utf-8
require 'rubygems'
require 'dbi'
require './list_user.rb'
require './quiz.rb'

class Base
  #Userクラスの初期化
  def initialize(id, name, age, percentage, db_name)
    @id = id
    @name = name
    @age = age
    @percentage = percentage
    #Baseクラスでのみコネクト処理
    #他の機能で使う場合は、@dbhを引数に渡してあげる
    @db_name = db_name
    @dbh = DBI.connect( "DBI:SQLite3:#{@db_name}")
  end

  #アクセサメソッド（セッター、ゲッター）を用意
  attr_accessor :id, :name, :age, :percentage
   
  #IDチェックメソッド
  def check
    #linux clearの処理　コンソール出力画面クリア処理
    puts `clear`
    puts "～～～～～～～～～～～～～～～～～～～～～～～～～～～
クイズランドへようこそ！
ゲームを始める前に、ログインしてね！
ユーザ登録がまだなら「a」を入力して登録しよう！
～～～～～～～～～～～～～～～～～～～～～～～～～～～～"
    puts "--------------ユーザID  or 「a」を入力してください--------------"
    $id = nil
    while $id == nil do
      print "\nユーザID: "
      #インスタンス変数に入力したidを格納
      @id = gets.chomp
      case @id
      when 'a'
        puts ""
        add
        break
      else
        #DBIのプレースホルダー
        sql = "select id from users where id = ?;"
        sth = @dbh.execute(sql, @id)
        sth.each do |row|
          row.each_with_name do |val, name|
            #グローバル変数に代入
            $id = val
            p "$id = #{$id}"
          end
        end
        #インスタンス変数の@idが空の文字列か@idに一致するid以外の場合
        print "idが違います。再度入力してください!" if @id == "" || @id != $id
        sth.finish
      end
    end
  end

  #データベースにユーザ登録を行う
  def add
    puts `clear`
    puts "--------------ユーザ情報を入力してください。-----------------"
    begin
      invalid_id = false
      print "ユーザIDを入力してください: "
      @id = gets.chomp
      if @id.empty? then
        print "入力が正しくありません！"
        invalid_id = true
      else
        sql = "select id from users where id = ?;"
        sth = @dbh.execute(sql, @id)
        #@idの場合はemptyのためeach_with_nameまで行かない
        sth.each do |row|
          row.each_with_name do |val, name|
            print "すでに使用されているIDです。"
            invalid_id = true
          end
        end
        sth.finish
      end
    end while invalid_id

    print "\nユーザ名: "
    @name = gets.chomp
    print "\n年齢: "
    @age = gets.chomp.to_i
    @percentage = 0
    #作成したユーザデータ1件分をデータベースに登録
    sql = "insert into users values(?, ?, ?, ?);"
    @dbh.do(sql, @id, @name, @age, @percentage)
    $id = @id
    puts "\n-------------------登録しました！----------------------\n"
  end
end

#トップメニューのクラスです。
class Manager < Base
  def run
    while true do
     puts `clear`
     puts "---------------------------------------------------" 
     puts "クイズを始めよう！！"
     user_info = List_user.new(@dbh)
     user_info.show_name
     print "
1. ユーザ登録
2. ゲームスタート
3. ユーザ詳細
4. ランキング
5. 終了
-----------------------------------------------------
番号を選んでください(1, 2, 3, 4, 5); "

      num = gets.chomp
      case num
      when '1'
        add
      when '2'
        quiztest = Quiz.new(@dbh)
        quiztest.question
      when '3'
        user_info.show_one 
      when '4'
        user_info.show_ranking
      when '5'
        break
      else
      end
    end
  end

end

app_manager = Manager.new("","",0,0,"app02.db")
app_manager.check
app_manager.run
