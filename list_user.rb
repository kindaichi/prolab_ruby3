# -*- coding: utf-8 -*-
require 'rubygems'
require 'dbi'

# これはデータベースのresultsテーブルからデータを読み込んで表示するプログラム
class List_user
  # 初期化メソッドである。
  # dbh: (DatabaseHandle) :             
  def initialize(dbh)  # initializeでdb_nameの初期化をしている
    # SQLiteデータファイルに接続するためのアクセスハンドラーを発行
    # @db_name = db_name
    # @dbh = DBI.connect( "DBI:SQLite3:#{@db_name}" )
    # item_nameでテーブル上の項目名を(user_idなど)を任意の形に変更
     @item_name = { 'id' => "ユーザーID",'name' => "ユーザー名", 
                    'age' => "年齢", 'percentage' => "正解率(%)" }
     @dbh = dbh
  end

  # データベースに格納されている値を出力する処理
  # sql: (String):
  def fetch_output(sql)
    puts "\nゲームの結果表示"
 
    puts "\n------------------------"
    # ここで上で発行したデータベースアクセスハンドルを使ってデータベースへ接続
    sth = @dbh.execute(sql)

    sth.each { |row|
      row.each_with_name { |val, name|
        puts "#{@item_name[name]}: #{val.to_s}"
      }
     puts "-------------------------"

    }
    sth.finish   # データベースとの接続終了
  
  end
  
  def show_one # こちらが個人の詳細情報出力表示
    puts "\e[H\e[2J"
    fetch_output("select * from users where id = '#{$id}'")
    print "終了は「enter」を押してください！"
    gets
  end

  def show_ranking # こちらがランキング出力表示
    puts "\e[H\e[2J"    
    fetch_output("select (
                    select count(percentage) + 1 from users as s
                    where s.percentage > m.percentage
                    ) || '位' as no,
                    * from users as m order by no desc")
    print "終了は「enter」を押してください！"
    gets
  end

  # topメニューで$idにひもづいたnameを表示する
  def show_name
     # dbiのプレースホルダ機構
     sql = "select name from users where id = ?;"
     sth = @dbh.execute(sql, $id)
     sth.each { |row|
      row.each_with_name { |val, name|
        @name = val
      }
    }
    sth.finish   # データベースとの接続終了
    puts "やあ！ #{@name} ！ 君は、何問とけるかな？"
  end
  

  private :fetch_output
end

